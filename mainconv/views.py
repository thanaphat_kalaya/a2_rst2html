from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.files import File
from django.core.files.storage import default_storage
import os, time

def home(request):
	return render(request,'home.html')

def rst2html(request):
	path = 'mainconv/uploadTemp/'
	name = request.POST['name']
	style = request.POST['style']
	file = request.FILES['uploadedFile']
	savePath = default_storage.save(path,file)
	os.rename(savePath,'{}upload.rst'.format(path))
	source = 'mainconv/uploadTemp/upload.rst'
	destination = 'mainconv/templates/outputTemp/' + name + '.html'
	if style == 'basic':
		os.system('rst2html5 ' + source + ' > ' + destination)
	elif style == 'deck':
		os.system('rst2html5 --deck-js --pretty-print-code --embed-content ' + source + ' > ' + destination)
	elif style == 'reveal':
		os.system('rst2html5 --jquery --reveal-js --pretty-print-code ' + source + ' > ' + destination)
	elif style == 'bootstrap':
		os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content ' + source + ' > ' + destination)
	return render(request,'outputTemp/'+name+'.html')

def filelist(request):
	filepath = 'mainconv/templates/outputTemp/'
	fl = []
	for file in os.listdir(filepath):
		filename,extention = os.path.splitext(file)
		if extention == '.html':
			fl.append(filename)
		fl.sort()
		context = {'filelist':fl}
	return render(request,'list.html',context)

def show(request,filename):
	return render(request,'outputTemp/'+filename+'.html')

def showedit(request,filename):
	return render(request,'previewTemp/'+filename+'.html')

def delete(request,filename):
	filepath = 'mainconv/templates/outputTemp/' + filename + '.html'
	os.remove(filepath)
	return HttpResponseRedirect(reverse('mainconv:filelist'))

def editor(request):
	return render(request,'editor.html')

def text2html(request):
	path = 'mainconv/uploadTemp/'
	name = request.POST['name']
	style = request.POST['style']
	f = open(path+'upload.rst', 'w')
	f.write(request.POST['text'])
	f.close()
	source = 'mainconv/uploadTemp/upload.rst'
	destination = 'mainconv/templates/outputTemp/' + name + '.html'
	if style == 'basic':
		os.system('rst2html5 ' + source + ' > ' + destination)
	elif style == 'deck':
		os.system('rst2html5 --deck-js --pretty-print-code --embed-content ' + source + ' > ' + destination)
	elif style == 'reveal':
		os.system('rst2html5 --jquery --reveal-js --pretty-print-code ' + source + ' > ' + destination)
	elif style == 'bootstrap':
		os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content ' + source + ' > ' + destination)
	return render(request,'outputTemp/'+name+'.html')

def text2preview(request):
	path = 'mainconv/uploadTemp/'
	name = request.POST['name']
	style = request.POST['style']
	f = open(path+'upload.rst', 'w')
	f.write(request.POST['text'])
	f.close()
	source = 'mainconv/uploadTemp/upload.rst'
	destination = 'mainconv/templates/previewTemp/' + name + '.html'
	if style == 'basic':
		os.system('rst2html5 ' + source + ' > ' + destination)
	elif style == 'deck':
		os.system('rst2html5 --deck-js --pretty-print-code --embed-content ' + source + ' > ' + destination)
	elif style == 'reveal':
		os.system('rst2html5 --jquery --reveal-js --pretty-print-code ' + source + ' > ' + destination)
	elif style == 'bootstrap':
		os.system('rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content ' + source + ' > ' + destination)
	return render(request,'previewTemp/'+name+'.html')

def upload_img_page(request):
	return render(request,'upload.html')

def upload_img(request):
	path = 'mainconv/static/images/'
	name = request.POST['name']
	file = request.FILES['uploadedFile']
	savePath = default_storage.save(path,file)
	os.rename(savePath,path+name)
	img = '/static/images/' + name
	context = {'img':img}
	return render(request,'preview.html',context)

