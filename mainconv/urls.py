from django.conf.urls import url
from . import views

app_name = 'mainconv'
urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'^rst2html/$', views.rst2html, name='rst2html'),
	url(r'^filelist/$', views.filelist, name='filelist'),
	url(r'^editor/$', views.editor, name='editor'),
	url(r'^text2html/$', views.text2html, name='text2html'),
	url(r'^text2preview/$', views.text2preview, name='text2preview'),
	url(r'^show/(?P<filename>[a-z,A-Z,0-9]+)', views.show, name='show'),
	url(r'^showedit/(?P<filename>[a-z,A-Z,0-9]+)', views.showedit, name='showedit'),
	url(r'^delete/(?P<filename>[a-z,A-Z,0-9]+)', views.delete, name='delete'),
	url(r'^upload/$', views.upload_img_page, name='upload_img_page'),
	url(r'^uploadimg/$', views.upload_img, name='upload_img')
]
